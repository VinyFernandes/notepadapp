import React, { Component } from 'react';
import { StyleSheet, View, Text, Modal, TextInput, TouchableOpacity } from 'react-native'

export default class ModalAddNote extends React.Component {

    state = {
        noteText: '',
    };

    onPressAddNote = () => {
        this.props.onAdd(this.state.noteText);
        this.setState({noteText: ''});
    };

    render(){
        return(
            <Modal visible={this.props.modalVisible} animationTYpe="fade" transparent={true} onRequestClose={this.props.onCancel}>
                <View style={styles.container}>

                    <View style={styles.innerContainer}>

                            <TextInput 
                                style={styles.textInput}
                                clearButtonMode = "always"
                                autoCapitalize='sentences'
                                autoCorrect={true}
                                blurOnSubmit={true}
                                multiline={true}
                                numberOfLines={5}
                                underlineColorAndroid='transparent'
                                value={this.state.noteText}
                                minHeight={50}
                                maxHeight={200}
                                keyboardType = 'ascii-capable'
                                autoFocus
                                onChangeText={(noteText) => this.setState({noteText})} 
                                placeholder='Criar uma nota...' 
                                placeholderTextColor='#333'
                                underlineColor='transparent' />

                        <View style={styles.btnContainer}>
                        
                            <TouchableOpacity onPress={this.onPressAddNote} style={[styles.btn, styles.btnSuccess]}>
                                <Text style={styles.btnText}>Criar</Text>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={this.props.onCancel} style={[styles.btn, styles.btnDefault]}>
                                <Text style={styles.btnTexDefault}>Cancelar</Text>
                            </TouchableOpacity>
                        </View>

                    </View>

                </View>
            </Modal>
        );
    };
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        height: 100,
        padding: 20,
    },
    innerContainer: {
        margin: 10,
        padding: 10,
        borderRadius: 10,
        backgroundColor: '#FFF',
        alignItems: 'center',
        justifyContent: 'center',
    },
    textInput: {
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'stretch',
        padding: 10,
        fontFamily: 'Roboto',
    },
    btnContainer: {
        marginTop: 50,
        height: 50,
        flexDirection: 'row',
    },
    btn: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 2,
        borderBottomWidth: 3,
        borderBottomColor: '#c8c5c0',
        borderRightWidth: 3,
        borderRightColor: '#c8c5c0',
        margin: 10,
        padding: 18,
    },
    btnText: {
        color: '#FFF',
        fontFamily: 'Roboto',
        fontWeight: 'normal',
        fontSize: 16,
    },
    btnTexDefault: {
        fontFamily: 'Roboto',
        fontWeight: 'normal',
        fontSize: 16,
    },
    btnSuccess: {
        backgroundColor: '#5cb85c',
    },
    btnInfo: {
        backgroundColor: '#428bca',
    },
    btnDanger: {
        backgroundColor: '#d9534f',
    },
    btnDefault: {
        backgroundColor: '#e0e0e0',
    },
});
