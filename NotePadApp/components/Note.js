import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';

export default class Note extends React.Component {

    render() { 
        return (

            <TouchableOpacity onPress={this.props.editMethod}>

                <View key={this.props.keyval} style={styles.note}>
                    
                    <Text style={[styles.noteText, styles.noteTextDate]}>{this.props.val.date}</Text>

                    <View style={styles.viewHorizontal}></View>
            
                    <Text style={styles.noteText}>{this.props.val.note}</Text>

                    <TouchableOpacity onPress={this.props.deleteMethod} style={styles.noteDelete}>
                        <Text style={styles.noteDeleteText}>x</Text>
                    </TouchableOpacity>

                </View>

            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    note:{
        backgroundColor: '#ffffff',
        position: 'relative',
        padding: 20,
        justifyContent: 'space-between',
        paddingRight: 100,
        borderBottomWidth: 2,
        borderBottomColor: '#e6e6e6',
        marginTop: 8,
        marginLeft: 10,
        marginRight: 10,
        borderRadius: 5,
        borderBottomWidth: 5,
        borderBottomColor: '#c8c5c0',
        borderRightWidth: 3,
        borderRightColor: '#c8c5c0',
    },
    noteTextDate: {
        fontWeight: 'bold',
        fontFamily: 'Roboto',
    },
    noteText:{
        paddingLeft: 20,
        paddingTop: 15,
        borderLeftWidth: 10,
        borderLeftColor: '#3eba84',
        fontFamily: 'Roboto',
    },
    viewHorizontal: {
        width: '100%',
        height: 1,
        backgroundColor: '#3eba84'
    },
    noteDelete:{
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#d9534f',
        padding: 10,
        top: 10,
        bottom: 10,
        right: 10,
        borderBottomWidth: 2,
        borderBottomColor: '#e6e6e6',
        borderRightWidth: 2,
        borderRightColor: '#e6e6e6',
    },
    noteDeleteText:{
        color: 'white',
        fontWeight: 'bold',
        fontFamily: 'Roboto',
        fontSize: 16,
    },
});
