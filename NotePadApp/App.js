import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, ScrollView, TouchableOpacity, AsyncStorage, Image } from 'react-native';

import Note from './components/Note';
import ModalAddNote from './components/ModalAddNote';
import ModalEditNote from './components/ModalEditNote';

export default class App extends React.Component {

  state = {
    modalAddNoteVisible: false,
    modalEditNoteVisible: false,
    noteToEdit: '',
    index: '',
    noteArray: []
  }

  async componentDidMount() {
    const noteArray = JSON.parse(await AsyncStorage.getItem("@NotePadApp:noteArray")) || [];
    this.setState({noteArray});
  }

  addNote = async (noteText) => {

    if (noteText.trim()) {

      var data = new Date();

      var days = [
        'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'
      ];

      var months = [
        'Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'
      ];

      const note = {
        date: days[data.getDay()-1] + ', ' + data.getDate() + ' de ' + months[data.getMonth()] + ' - ' + data.getFullYear()
          + '\t' + data.getHours() + ':' + data.getMinutes() + ':' + data.getSeconds(),
        note: noteText
      }

      var arr = [...this.state.noteArray];
      arr.unshift(note);

      this.setState({
        modalAddNoteVisible: false,
        modalEditNoteVisible: false,
        noteArray: arr
      });
      
      await AsyncStorage.setItem("@NotePadApp:noteArray", JSON.stringify([...this.state.noteArray, note]));
    }
  };

  deleteNote = async (key) => {
    this.state.noteArray.splice(key, 1);
    this.setState({noteArray: this.state.noteArray});
    await AsyncStorage.setItem("@NotePadApp:noteArray", JSON.stringify([...this.state.noteArray]));
  }

  editNote = async (noteText) => {

    if (noteText.trim()) {
      const arr = [...this.state.noteArray];
      arr[this.state.index].note = noteText;

      this.setState({
        noteArray: arr
      });

      await AsyncStorage.setItem("@NotePadApp:noteArray", JSON.stringify([...this.state.noteArray]));
    }

    this.setState({
      modalEditNoteVisible: false,
    });
  }

  OnPressEditNote = (note, index) => {
    this.state.noteToEdit = note.note;
    this.state.index = index;
    this.setState({modalEditNoteVisible: true});
  }

  render() {

    notes = this.state.noteArray.map((val, key) => {
      return <Note key={key} keyval={key} val={val} deleteMethod={ ()=> this.deleteNote(key)} editMethod={ ()=> this.OnPressEditNote(val, key)} />
    });

    return (

      <View style={styles.container}>

          <View style={styles.header}>
            <TouchableOpacity onPress={()=>{this.setState({modalVisible: true})}}>
              <Image 
                style={styles.headerMenu}
                source={require('./img/menu-button.png')}
              />
            </TouchableOpacity>
            <Text style={styles.headerText}>Notas</Text>
            
          </View>

          <ScrollView style={styles.scrollContainer}>
              {notes}
          </ScrollView>

          <View style={styles.footer}>

              <TouchableOpacity onPress={ ()=> {this.setState({modalAddNoteVisible: true})}} style={styles.addButtom}>
                  <Text style={styles.addButtomText}>+</Text>
              </TouchableOpacity>

              <View style={styles.footerContainer}></View>

          </View>

          <ModalAddNote modalVisible={this.state.modalAddNoteVisible} 
            onCancel={ ()=> this.setState({modalAddNoteVisible: false}) }
            onAdd={this.addNote}
          />
          
          <ModalEditNote 
            modalVisible={this.state.modalEditNoteVisible} 
            noteText={this.state.noteToEdit}
            onChangeText={ (text) => this.setState({noteToEdit : text}) }
            onEdit={this.editNote}
            onCancel={ ()=> this.setState({modalEditNoteVisible: false})}  />

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EDEDED',
  },
  header:{
    backgroundColor: '#3eba84',
    //backgroundColor: '#ffbb00',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 5,
    borderBottomColor: '#c8c5c0',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headerText:{
    textAlign: 'left',
    color: '#ffffff',
    fontSize: 20,
    padding: 20,
    marginTop: 20,
    marginRight: 220,
    marginLeft: 20,
    fontWeight: 'bold',
    fontFamily: 'Roboto',
  },
  headerMenu: {
    width: 18,
    height: 18,
    marginTop: 20,
    marginLeft: 20,
  },
  scrollContainer:{
    flex: 1,
    marginBottom: 5,
    marginTop: 2,
  },
  footer:{
    position: 'absolute',
    alignItems: 'center',
    bottom: 0,
    left: 0,
    right: 0,
  },
  addButtom:{
    backgroundColor: '#3eba84',
    width: 60,
    height: 60,
    borderRadius: 50,
    borderBottomColor: '#c8c5c0',
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 10,
    marginLeft: 180,
    zIndex: 10,
  },
  addButtomText:{
    color: '#ffffff',
    fontSize: 35,
    fontFamily: 'serif',
    fontWeight: 'normal',
  },
  footerContainer:{
    alignSelf: 'stretch',
    padding: 20,
    paddingTop: 35,
    backgroundColor: 'transparent',
  },
});
